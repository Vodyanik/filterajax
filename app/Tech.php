<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tech extends Model
{
    public function products()
    {
        return $this->hasMany(Product::class,'tech_id', 'id');
    }
}
