<?php

namespace App\Http\Controllers;

use App\Author;
use App\Category;
use App\Product;
use App\Tech;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * @var Product
     */
    private $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function index(Request $request)
    {
        $model = $this->product;
        $perPage = $request->get('perPage', 6);

        if ($request->exists('authors')) {
            $authors = $request->get('authors');
            $model = $model->whereIn('author_id', $authors);
//            $model = $model->whereHas('authors', function ($query) use ($authors) {
//                $query->whereIn('id', $authors);
//            });
        }
        if ($request->exists('categories')) {
            $categories = $request->get('categories');
            $model = $model->whereHas('categories', function ($query) use ($categories) {
                $query->whereIn('id', $categories);
            });
        }
        if ($request->exists('techs')) {
            $techs = $request->get('techs');
            $model = $model->whereHas('techs', function ($query) use ($techs) {
                $query->whereIn('id', $techs);
            });
        }
        if ($request->exists('amount_from') && $request->exists('amount_to')) {
            $amountFrom = $request->get('amount_from');
            $amountTo = $request->get('amount_to');
            if ($amountTo >= 0 && $amountFrom >= 0) {
                $model = $model->whereBetween('price', [$amountFrom, $amountTo]);
            }
        }
        if ($request->exists('sort') || $request->exists('sortTo')) {
            $sortBy = $request->get('sort');
            if ($sortBy == 'newest') {
                $model = $model->orderBy('id', 'desc');
            } else if ($sortBy == 'oldest') {
                $model = $model->orderBy('id', 'asc');
            } else if ($sortBy == 'priceup') {
                $model = $model->orderBy('price', 'asc');
            } else if ($sortBy == 'pricedown') {
                $model = $model->orderBy('price', 'desc');
            }
        }
        $model = $model->paginate($perPage);

        if ($request->ajax()) {
            return view('products-list', ['products' => $model]);
        }

        $categories = Category::all();
        $teches = Tech::all();
        $authors = Author::all();
        $products = $model;

        return view('products', compact('categories', 'teches', 'authors', 'products'));

    }

}
