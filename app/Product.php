<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable = [
        'price',
        'name'
    ];

    public function authors()
    {
        return $this->belongsTo(Author::class, 'author_id', 'id');
    }

    public function categories()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function techs()
    {
        return $this->belongsTo(Tech::class, 'tech_id', 'id');
    }

}
