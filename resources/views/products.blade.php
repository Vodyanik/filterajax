<!doctype html>
<html lang="ru">
<head>
    <title>Title</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="noindex, nofollow">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <link rel="stylesheet" href="{{ asset('assets/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/justifiedgallery.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/colorbox.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">

    <!-- <link href="favicon.ico" rel="shortcut icon" type="image/x-icon"> -->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script> window.jQuery || document.write('<script src="/js/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="{{ asset('assets/js/migrate.js') }}"></script>
    <script src="{{ asset('assets/js/svg4everybody.min.js') }}"></script>
    <script src="{{ asset('assets/js/slick.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.formstyler.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/js/wow.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.justifiedgallery.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.colorbox-min.js') }}"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>
    <script>
        new WOW().init();
    </script>

    <!--[if IE]>
    <script src="{{ asset('assets/js/html5shiv.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.placeholder.min.js') }}"></script>
    <script>$(document).ready(function(){$('input, textarea').placeholder()})</script>
    <![endif]-->
</head>
<body class="topPadidng">
    <header class="header">
        <div class="header_top">
            <div class="wrapper">
                <div class="navigation_btn"><span></span></div>
                <a href="index.php" class="header_logo"><img src="img/logo.png" alt="РосАрт"></a>
                <ol class="header_right">
                    <li><a href="#" onclick="$('.modal_login').dialog();return false;"><svg class="icon icon-login"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/svgdefs.svg#icon-login"></use></svg></a></li>
                    <li><a href="#" onclick="$('.modal_registration').dialog();return false;"><svg class="icon icon-user"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/svgdefs.svg#icon-user"></use></svg></a></li>
                    <li class="header_lang || js_DropWrap">
                        <div class="ttl || js_DropBtn">Ru <i class="ic_arrow"></i></div>
                        <div class="box || js_DropBox">
                            <a href="index.php">En</a>
                            <a href="index.php">En</a>
                            <a href="index.php">En</a>
                        </div>
                    </li>
                    <li class="cart"><a href="cart.php"><svg class="icon icon-cart"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/svgdefs.svg#icon-cart"></use></svg></a></li>
                </ol>
            </div>
        </div>
        <div class="navigation_wrap">
            <div class="wrapper">
                <nav class="navigation">
                    <ol class="nav_btns">
                        <li><a href="#" onclick="$('.modal_login').dialog();return false;"><svg class="icon icon-login"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/svgdefs.svg#icon-login"></use></svg></a></li>
                        <li><a href="#" onclick="$('.modal_registration').dialog();return false;"><svg class="icon icon-user"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/svgdefs.svg#icon-user"></use></svg></a></li>
                        <li class="header_lang || js_DropWrap">
                            <div class="ttl || js_DropBtn">Ru <i class="ic_arrow"></i></div>
                            <div class="box || js_DropBox">
                                <a href="index.php">En</a>
                                <a href="index.php">En</a>
                                <a href="index.php">En</a>
                            </div>
                        </li>
                    </ol>
                    <form action="#" class="header_search">
                        <input type="text" class="input" placeholder="Поиск">
                        <button><svg class="icon icon-search"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/svgdefs.svg#icon-search"></use></svg></button>
                    </form>
                    <ul>
                        <li id="active_menu_1"><a href="about.php">О галерее</a></li>
                        <li id="active_menu_2"><a href="masters.php">Мастера</a></li>
                        <li id="active_menu_3" class="dropDown">
                            <a href="catalog.php">Каталог <i class="ic_arrow"></i></a>
                            <ul class="navigation_dropDown">
                                <li><a href="category.php">Живопись</a></li>
                                <li><a href="category.php">Графика</a></li>
                                <li><a href="category.php">Изделия</a></li>
                            </ul>
                        </li>
                        <li id="active_menu_4"><a href="blog.php">Блог</a></li>
                        <li id="active_menu_5"><a href="contacts.php">Контакты</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="dark_overlay"></div>
    </header>
    <div class="adb_pic_wd" style="background-image: url({{ asset('assets/img/category_bg.jpg') }});">
        <div class="wrapper">
            <ul class="breadcrumbs">
                <li><a class="underscore" href="index.php">Главная</a></li>
                <li><a class="underscore" href="index.php">Каталог</a></li>
                <li><span>Живопись</span></li>
            </ul>
        </div>
    </div>
    <section class="category">
        <div class="wrapper">
            <h1 class="title">Каталог</h1>
            <div class="category_nav">
                <a href="category.php" class="button || active"><span>Живопись</span><i class="ic_dir_arrow"></i></a>
                <a href="category.php" class="button"><span>Графика</span><i class="ic_dir_arrow"></i></a>
                <a href="category.php" class="button"><span>Изделия</span><i class="ic_dir_arrow"></i></a>
            </div>
            <div class="rows">
                <!-- CATEGORY SIDEBAR -->
                <aside class="category_sidebar">
                    <form action="{{ route('products.index') }}" id="filter_form" onsubmit="event.preventDefault()" method="get">
                        <h3 class="h3">Показать</h3>
                        <div class="filterWrap isOpened">
                            <div class="filterHead">Цена</div>
                            <div class="filterBody filterSlider">
                                <div id="filter_price"></div>
                                <div class="rows">
                                    <input type="text" id="amount_from" name="amount_from" class="from" readonly style="border:0;">
                                    <input type="text" id="amount_to" name="amount_to" class="to" readonly style="border:0;">
                                </div>
                            </div>
                        </div>
                        <div class="filterWrap isOpened">
                            <div class="filterHead">Мастер</div>
                            <ul class="filterBody">
                                @foreach($authors as $author)
                                    <li>
                                        <input type="checkbox" name="authors[]" value="{{ $author->id }}" {{ in_array($author->id, request('authors', [])) ? 'checked' : '' }} class="checkbox" id="author_{{ $author->id }}">
                                        <label for="author_{{ $author->id }}">{{ $author->name }}</label>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="filterWrap isOpened">
                            <div class="filterHead">Категория</div>
                            <ul class="filterBody">
                                @foreach($categories as $category)
                                    <li>
                                        <input type="checkbox" name="categories[]" value="{{ $category->id }}" {{ in_array($category->id, request('categories', [])) ? 'checked' : '' }} class="checkbox" id="category_{{ $category->id }}">
                                        <label for="category_{{ $category->id }}">{{ $category->name }}</label>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="filterWrap isOpened">
                            <div class="filterHead">Техника</div>
                            <ul class="filterBody">
                                @foreach($teches as $tech)
                                    <li>
                                        <input type="checkbox" name="techs[]" value="{{ $tech->id }}" {{ in_array($tech->id, request('techs', [])) ? 'checked' : '' }} class="checkbox" id="tech_{{ $tech->id }}">
                                        <label for="tech_{{ $tech->id }}">{{ $tech->name }}</label>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </form>
                </aside>
                <!-- CATEGORY CONTENT -->
                <div class="category_content">
                    <div class="rows">
                        <select name="sort" class="filter_sort_by" data-placeholder="Сортировка">
                            <option value=""></option>
                            <option value="newest">Новейшие</option>
                            <option value="oldest">staroe</option>
                            <option value="priceup">По возрастанию цены</option>
                            <option value="pricedown">По убыванию цены</option>
                            <option value="">Название А-Я</option>
                            <option value="">Название Я-А</option>
                        </select>
                    </div>
                    <div class="category_items">
                        @include('products-list')
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer class="footer" style="background-image: url({{ asset('assets/img/footer.jpg') }});">
        <div class="wrapper">
            <div class="footer_content">
                <div class="column cl1">
                    <a href="index.php" class="footer_logo">
                        <img src="img/logo.png" alt="РосАрт">
                    </a>
                    <p>Искусство русских мастеров</p>
                </div>
                <div class="column cl2">
                    <h3 class="hh">Каталог</h3>
                    <ul class="footer_nav">
                        <li id="active_menu_101"><a class="underscore" href="category.php">Живопись</a></li>
                        <li id="active_menu_102"><a class="underscore" href="category.php">Графика</a></li>
                        <li id="active_menu_103"><a class="underscore" href="category.php">Изделия</a></li>
                    </ul>
                </div>
                <div class="column cl3">
                    <h3 class="hh">О компании</h3>
                    <ul class="footer_nav">
                        <li id="active_menu_104"><a class="underscore" href="blog_popular.php">Популярные статьи</a></li>
                        <li id="active_menu_105"><a class="underscore" href="how_buy.php">Как купить</a></li>
                    </ul>
                </div>
                <div class="column cl4">
                    <h3 class="hh">Присоединяйтесь к нам</h3>
                    <div class="social">
                        <a href="#" target="_blank"><svg class="icon icon-facebook"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/svgdefs.svg#icon-facebook"></use></svg></a>
                        <a href="#" target="_blank"><svg class="icon icon-vk"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/svgdefs.svg#icon-vk"></use></svg></a>
                    </div>
                </div>
            </div>
            <div class="footer_copy">© РосАрт 2016-2017</div>
            <div class="btn_up_wrap"><div class="btn_up"></div></div>
        </div>
    </footer>
    <div class='dialog_wrapper'>
        <div class='table || main_table'>
            <div class='tcell || main_tcell'>
                <div class='dialog_close || dialog_bg'></div>

                <!-- заказать звонок -->
                <div class="modal_feedback dialog">
                    <span class='dialog_close || icon_close'><svg class='icon'><use xlink:href='img/svgdefs.svg#icon_close'></use></svg></span>
                    <form action="#" class="feedback">
                        <h2 class="title">Заказать звонок</h2>
                        <p class="mp1">Возникли вопросы, оставьте заявку и мы с Вами свяжемся</p>
                        <div class="input_effect">
                            <input name="" type="text" class="input">
                            <label class="labels">Как Вас зовут?</label>
                        </div>
                        <div class="input_effect">
                            <input id="" name="" type="email" class="input">
                            <label class="labels">Эллектронная почта</label>
                        </div>
                        <input name="" type="text" class="input" placeholder="+ _ _ ( _ _ _ ) _ _ _ - _ _ - _ _">
                        <div class="input_effect">
                            <textarea name="" class="input"></textarea>
                            <label class="labels">Какой у Вас вопрос?</label>
                        </div>
                        <button class="button btn_wd">Перезвоните мне</button>
                    </form>
                </div>

                <!-- успешная подписка -->
                <div class="modal_alert_book dialog">
                    <span class='dialog_close || icon_close'><svg class='icon'><use xlink:href='img/svgdefs.svg#icon_close'></use></svg></span>
                    <span class="ico_check"></span>
                    <p>Вы успешно подписаны на рассылку</p>
                </div>

                <!-- регистрация -->
                <div class="modal_registration dialog">
                    <span class='dialog_close || icon_close'><svg class='icon'><use xlink:href='img/svgdefs.svg#icon_close'></use></svg></span>
                    <form action="#" class="login_form">
                        <div class="title">Регистрация</div>
                        <div class="rows">
                            <button class="button btn_fb">Facebook</button>
                            <button class="button btn_gp">Google +</button>
                        </div>
                        <label class="labels">Имя</label>
                        <input type="text" class="input">
                        <label class="labels">Электронная почта</label>
                        <input type="email" class="input">
                        <label class="labels">Номер телефона</label>
                        <input type="text" class="input">
                        <label class="labels">Пароль</label>
                        <input type="password" class="input">
                        <button class="button btn_wd" style="margin: 1.2rem 0 1rem;">ЗАРЕГИСТРИРОВАТЬСЯ</button>
                        <p class="mp2">Уже зарегистрированы?</p>
                        <div class="tc"><a href="#" class="btn_open_login || underscore_rev">Войти</a></div>
                    </form>
                </div>

                <!-- вход -->
                <div class="modal_login dialog">
                    <span class='dialog_close || icon_close'><svg class='icon'><use xlink:href='img/svgdefs.svg#icon_close'></use></svg></span>
                    <form action="#" class="login_form">
                        <div class="title">Вход</div>
                        <div class="rows">
                            <button class="button btn_fb">Facebook</button>
                            <button class="button btn_gp">Google +</button>
                        </div>
                        <label class="labels">Электронная почта</label>
                        <input type="email" class="input">
                        <label class="labels">Пароль</label>
                        <input type="password" class="input">
                        <button class="button btn_wd" style="margin: 1.2rem 0 1rem;">Войти</button>
                        <p class="mp2">Ещё не  зарегистрированы?</p>
                        <div class="tc"><a href="#" class="btn_open_reg || underscore_rev">Зарегистрироваться</a></div>
                    </form>
                </div>


                <div class="modal_ dialog">
                    <span class='dialog_close || icon_close'><svg class='icon'><use xlink:href='img/svgdefs.svg#icon_close'></use></svg></span>
                    <p>Modal content</p>
                </div>
            </div>
        </div>
    </div>

    <script>

        var filter_from = $('#filter_form');
        $(document).ready(function () {
            $(".checkbox").click(function () {
                $(this).attr('checked', true)
                var url = filter_from.attr('action') + '?';
                url += cleanUrlOfEmptyParamsForm(filter_from);
                history.pushState({}, '', url);

                $.ajax({
                    method: "GET",
                    url: url,
                    success: function (data) {
                        $('.category_items').empty();
                        $('.category_items').append(data);
                    }
                });
            });
        });
        $('.filter_sort_by').change(function () {
            var location = window.location.href;
            var sortText = '?&sort=';
            var url = location.replace(sortText, '');
            window.location.href = url + '?&sort=' + $(this).val();
        });

    </script>
</body>
</html>