<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Author::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'slug' => str_slug($faker->name()),
    ];
});

$factory->define(App\Category::class, function (Faker $faker) {
    return [
        'name' => $faker->text(20),
        'slug' => str_slug($faker->text(20)),
    ];
});

$factory->define(App\Tech::class, function (Faker $faker) {
    return [
        'name' => $faker->text(20),
        'slug' => str_slug($faker->text(20)),
    ];
});

//$factory->define(App\Product::class, function (Faker $faker) use ($factory) {
//    return [
//        'name' => $faker->text(20),
//        'image' => $faker->image(public_path('images')),
//        'price' => rand(100, 1000),
//        'author_id' => function() {
//            return factory(App\Author::class)->create()->id;
//        },
//        'category_id' => function() {
//            return factory(App\Category::class)->create()->id;
//        },
//        'tech_id' => function() {
//            return factory(App\Tech::class)->create()->id;
//        },
//    ];
//});

$factory->define(App\Product::class, function (Faker $faker) use ($factory) {
    return [
        'name' => $faker->text(20),
//        'image' => $faker->image(public_path('images')),
        'image' => $faker->imageUrl(),
        'price' => rand(100, 1000),
        'author_id' => rand(1, 10),
        'category_id' => rand(1, 10),
        'tech_id' => rand(1, 10),
    ];
});
