<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        factory(App\Author::class, 10)->create();
        factory(App\Category::class, 10)->create();
        factory(App\Tech::class, 10)->create();
        factory(App\Product::class, 30)->create();
    }
}
